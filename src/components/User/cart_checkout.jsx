import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import FormField from '../utils/form/formFields';
import { update, generateData, isFormValid } from '../utils/form/formActions';
// import { registerUser } from '../../actions/user_actions';

class CartCheckOut extends Component {

    state = {
        formError: false,
        formSuccess: false,
        formdata: {
            street: {
                element: 'input',
                value: '',
                config: {
                    name: 'street_input',
                    type: 'text',
                    placeholder: 'Street Address'
                },
                validation: {
                    required: true
                },
                valid: true,
                touched: true,
                validationMessage: ''
            },
            city: {
                element: 'input',
                value: '',
                config: {
                    name: 'city_input',
                    type: 'text',
                    placeholder: 'City'
                },
                validation: {
                    required: true
                },
                valid: true,
                touched: true,
                validationMessage: ''
            },
            zip: {
                element: 'input',
                value: '',
                config: {
                    name: 'zip_input',
                    type: 'text',
                    placeholder: 'ZIP / Postal Code'
                },
                validation: {
                    required: true
                },
                valid: true,
                touched: true,
                validationMessage: ''
            },
            card: {
                element: 'input',
                value: '',
                config: {
                    name: 'card_input',
                    type: 'text',
                    placeholder: 'Credit Card Number'
                },
                validation: {
                    required: true
                },
                valid: true,
                touched: true,
                validationMessage: ''
            },
            expiration: {
                element: 'input',
                value: '',
                config: {
                    name: 'expiration_input',
                    type: 'text',
                    placeholder: 'Expiration Date'
                },
                validation: {
                    required: true
                },
                valid: true,
                touched: true,
                validationMessage: ''
            },
            cvv: {
                element: 'input',
                value: '',
                config: {
                    name: 'cvv_input',
                    type: 'text',
                    placeholder: 'Security Code'
                },
                validation: {
                    required: true
                },
                valid: true,
                touched: true,
                validationMessage: ''
            }
        }
    }

    render() {
        return (
            <div className="check_wrapp">
                <form onSubmit={(event) => this.submitForm(event)}>
                    <h2>Adress</h2>
                    <div className="form-row">
                        <div className="form-group col-4">
                            <FormField
                                id={'street'}
                                formdata={this.state.formdata.street}
                                change={(element) => this.updateForm(element)}
                            />
                        </div>
                        <div className="form-group col-4">
                            <FormField
                                id={'city'}
                                formdata={this.state.formdata.city}
                                change={(element) => this.updateForm(element)}
                            />
                        </div>
                        <div className="form-group col-4">
                            <FormField
                                id={'zip'}
                                formdata={this.state.formdata.zip}
                                change={(element) => this.updateForm(element)}
                            />
                        </div>
                    </div>
                    <h2>Card Information</h2>
                    <div className="form-row">
                        <div className="form-group col-4">
                            <FormField
                                id={'card'}
                                formdata={this.state.formdata.card}
                                change={(element) => this.updateForm(element)}
                            />
                        </div>
                        <div className="form-group col-4">
                            <FormField
                                id={'expiration'}
                                formdata={this.state.formdata.expiration}
                                change={(element) => this.updateForm(element)}
                            />
                        </div>
                        <div className="form-group col-4">
                            <FormField
                                id={'cvv'}
                                formdata={this.state.formdata.cvv}
                                change={(element) => this.updateForm(element)}
                            />
                        </div>
                    </div>
                    {
                        this.state.formError ?
                            <div className="error_label">
                                Please check your data
                                    </div>
                            :
                            null
                    }
                    <button onClick={(event) => this.submitForm(event)} type="submit" className="btn btn-primary">Create</button>
                </form>
            </div>
        );
    }
}

export default CartCheckOut;